/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <render_util/image.h>
#include <render_util/terrain_base.h>

#include <vector>

namespace il2ge
{

  namespace water_map
  {
    enum ChunkType
    {
      CHUNK_EMPTY = 0,
      CHUNK_FULL,
      CHUNK_MIXED
    };

    constexpr int CHUNK_SIZE_PX = 32;
    constexpr int CHUNK_SIZE_TERRAIN_UNITS = 8;
    constexpr int CHUNK_SIZE_M = CHUNK_SIZE_TERRAIN_UNITS * render_util::TerrainBase::GRID_RESOLUTION_M;
    static_assert(CHUNK_SIZE_M == 1600);
    constexpr float CHUNK_BORDER_PX = 0.5;
    constexpr glm::ivec2 OFFSET_TERRAIN_UNITS = glm::ivec2(0, 1);
  }

  struct WaterMap
  {
    render_util::ImageGreyScale::ConstPtr chunks;
    render_util::Image<unsigned int>::ConstPtr table;
  };

  void processWaterMap(const WaterMap &src,
                       WaterMap &dst,
                       render_util::Image<water_map::ChunkType>::Ptr &small_map);

}
