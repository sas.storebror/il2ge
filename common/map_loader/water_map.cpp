/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "water_map.h"
#include <render_util/image_util.h>
#include <render_util/terrain_base.h>

#include <glm/glm.hpp>
#include <iostream>

using namespace glm;
using namespace std;

namespace
{


using namespace il2ge;
using namespace il2ge::water_map;


render_util::ImageGreyScale::Ptr createEmptyImage(int size)
{
  return render_util::image::create<unsigned char>(0, ivec2(size));
}


render_util::ImageGreyScale::Ptr createFullImage(int size)
{
  return render_util::image::create<unsigned char>(255, ivec2(size));
}


void splitChunks(render_util::ImageGreyScale::ConstPtr image,
                  int chunk_size,
                  std::vector<render_util::ImageGreyScale::ConstPtr> &chunks)
{
  int chunks_per_row = image->w() / chunk_size;
  int num_rows = image->h() / chunk_size;

  for (int y = 0; y < num_rows; y++)
  {
    for (int x = 0; x < chunks_per_row; x++)
    {
      auto chunk = render_util::image::subImage(image.get(),
                                                x * chunk_size,
                                                y * chunk_size,
                                                chunk_size,
                                                chunk_size);

      chunk = render_util::image::flipY(chunk);
      chunks.push_back(chunk);
    }
  }
}


ChunkType classifyChunk(render_util::ImageGreyScale::ConstPtr image)
{
  int num_empty = 0;
  int num_full = 0;
  int num_other = 0;

  for (int y = 0; y < image->w(); y++)
  {
    for (int x = 0; x < image->h(); x++)
    {
      unsigned int color = image->get(x,y);

      if (color == 0)
        num_empty++;
      else if (color == 255)
        num_full++;
      else
        num_other++;
    }
  }

  if (num_other)
    return CHUNK_MIXED;
  else
  {
    if (num_full && !num_empty)
      return CHUNK_FULL;
    else if (num_empty && !num_full)
      return CHUNK_EMPTY;
    else
      return CHUNK_MIXED;
  }
}


class Chunk
{
  render_util::ImageGreyScale::ConstPtr m_image;
  ChunkType m_type = CHUNK_MIXED;

public:
  ChunkType getType() { return m_type; }

  bool isEmpty() const
  {
    return m_type == CHUNK_EMPTY;
  }

  bool isFull() const
  {
    return m_type == CHUNK_FULL;
  }

  void setEmpty()
  {
    m_type = CHUNK_EMPTY;
    m_image = nullptr;
  }

  void setFull()
  {
    m_type = CHUNK_FULL;
    m_image = nullptr;
  }

  void setType(ChunkType type)
  {
    m_type = type;
    if (type != CHUNK_MIXED)
      m_image = nullptr;
  }

  render_util::ImageGreyScale::ConstPtr getImage()
  {
    assert(!isEmpty());
    assert(!isFull());
    assert(m_image);

    return m_image;
  }

  void setImage(render_util::ImageGreyScale::ConstPtr image)
  {
    assert(image);
    m_type = CHUNK_MIXED;
    m_image = image;
  }
};


class Map
{
  render_util::Array2D<Chunk> chunks;

public:
  Map(int w, int h) : chunks(w, h) {}

  Map(const WaterMap &src) : chunks(src.table->w(), src.table->h())
  {
    std::vector<render_util::ImageGreyScale::ConstPtr> chunks_in;
    splitChunks(src.chunks, water_map::CHUNK_SIZE_PX, chunks_in);

    auto table = render_util::image::flipY(src.table);

    for (int y = 0; y < table->h(); y++)
    {
      for (int x = 0; x < table->w(); x++)
      {
        unsigned int index = table->get(x,y);
        assert(index < chunks_in.size());

        Chunk &dst_chunk = chunks.at(x, y);
        render_util::ImageGreyScale::ConstPtr image = chunks_in[index];

        dst_chunk.setImage(image);
        dst_chunk.setType(classifyChunk(image));
      }
    }
  }

  Chunk *getChunk(ivec2 pos)
  {
    if (pos.x < 0 || pos.y < 0)
    {
      return nullptr;
    }
    if (pos.x < chunks.w() && pos.y < chunks.h())
    {
      return &chunks.at(pos.x, pos.y);
    }
    else
    {
      return nullptr;
    }

  }

  int w() { return chunks.w(); }
  int h() { return chunks.h(); }
};


render_util::Image<ChunkType>::Ptr createSmallMap(Map &map)
{
  constexpr int pixel_per_chunk = CHUNK_SIZE_M / render_util::TerrainBase::GRID_RESOLUTION_M;
  static_assert(CHUNK_SIZE_M % render_util::TerrainBase::GRID_RESOLUTION_M == 0);
  static_assert(pixel_per_chunk == 8);

  const ivec2 size = ivec2(map.w(), map.h()) * pixel_per_chunk;

  auto dst_map = make_shared<render_util::Image<ChunkType>>(size);

  for (int y = 0; y < dst_map->h(); y++)
  {
    for (int x = 0; x < dst_map->w(); x++)
    {
      ivec2 chunk_coords = ivec2(x,y) / pixel_per_chunk;
      Chunk *chunk = map.getChunk(chunk_coords);

      dst_map->at(x,y) = chunk->getType();
    }

  }

  return dst_map;
}


} // namespace


namespace il2ge
{


void processWaterMap(const WaterMap &src,
                     WaterMap &dst,
                     render_util::Image<ChunkType>::Ptr &small_map)
{
  Map src_map(src);

  small_map = createSmallMap(src_map);
  dst = src;
}


} // namespace il2ge
