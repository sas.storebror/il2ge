# Changelog

## [Unreleased]

## [0.2.0]

### Fixed
- #20 - Bombsight has wrong color
- #25 - Shadows on water have wrong color
- #14 - Lines are displayed on water and land

## [0.1.0]

- First release
